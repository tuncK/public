#!/bin/sh
# Quick installation script for Linux Mint, removes/installs packages would be required by a typical user. 
# Adjusted for Ubuntu Mint 19.1 Tessa

# Update repositories
sudo apt update

# Remove unnecessary components that the installation includes by default
sudo apt remove --purge thunderbird hexchat tomboy pix rhythmbox timeshift transmission-common transmission-gtk -y
sudo apt remove speech-dispatcher speech-dispatcher-audio-plugins libspeechd2 libespeak* -y
sudo apt remove mintupdate -y
sudo apt autoremove -y

# Remove unused folders and associated bookmarks
rm -r ~/Documents ~/Music ~/Videos ~/Public ~/Templates ~/Pictures
bookmarkConfig="./.config/gtk-3.0/bookmarks"
sed -i '/Documents/d; /Music/d; /Videos/d; /Pictures/d' $bookmarkConfig
cat $bookmarkConfig

# Set some convenient to use settings
dconf write /org/cinnamon/desktop/interface/first-day-of-week 1 # i.e. monday
dconf write /org/cinnamon/desktop/interface/clock-show-date true
dconf write /org/cinnamon/desktop/background/slideshow/slideshow-enabled true
dconf write /org/cinnamon/desktop/background/slideshow/image-source "'xml:///usr/share/cinnamon-background-properties/linuxmint-tina.xml'"

# Laptop specific settings
dconf write /org/cinnamon/desktop/screensaver/lock-enabled false
dconf write /org/cinnamon/settings-daemon/peripherals/touchpad/scrolling-method 2 # single finger scrolling
dconf write /org/cinnamon/settings-daemon/peripherals/touchpad/natural-scroll false # scroll direction


# Text editor (ex. Xed) configuration
dconf write /org/x/editor/preferences/editor/insert-spaces false
dconf write /org/x/editor/preferences/editor/bracket-matching true
dconf write /org/x/editor/preferences/editor/scheme "'cobalt'"
dconf write /org/x/editor/preferences/editor/highlight-current-line true
dconf write /org/x/editor/preferences/editor/display-line-numbers true
dconf write /org/x/editor/preferences/editor/auto-indent true


# Install 3rd party codecs (necessary for websites and playing video files)
# sudo apt install adobe-flashplugin -y
sudo apt install gstreamer1.0-plugins-bad gstreamer1.0-libav -y
# Option? sudo apt install mint-meta-codecs

# Install typical scientific computation tools
if true; then
	# Install Octave. Second line provides some extra packages Octave complains about during image export.
	sudo apt install octave liboctave-dev epstool pstoedit transfig -y
	sudo apt install g++ -y
	
	# Install Latex compiler
	sudo apt install texlive-latex-base texlive-latex-recommended texlive-science texlive-latex-extra -y
	sudo apt install inkscape -y	
fi

# Install chromium ~= google chrome
# sudo apt install chromium-browser -y

# Update the system and then remove the unpacked packages
sudo apt dist-upgrade -y
sudo apt clean


# Generate public encryption key pair (necessary for server authentications)
if false; then
	sudo apt install autossh
	ssh-keygen -t rsa -b 4096 <<- EOF
	
	
	
	EOF
fi
