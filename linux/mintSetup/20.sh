#!/bin/sh
# Quick installation script for Linux Mint, removes/installs packages would be required by a typical user. 
# Adjusted for Ubuntu Mint 20

# Update repositories
sudo apt update

# Remove unnecessary components that the installation includes by default
removeList=()
removeList+=(hexchat pix rhythmbox timeshift transmission-common transmission-gtk xreader )
removeList+=(thunderbird )
removeList+=(mintupdate warpinator )
sudo apt remove --purge ${removeList[@]} -y
sudo apt autoremove -y

# Remove unused folders and associated bookmarks
rm -r ~/Documents ~/Music ~/Videos ~/Public ~/Templates ~/Pictures
bookmarkConfig="./.config/gtk-3.0/bookmarks"
sed -i '/Documents/d; /Music/d; /Videos/d; /Pictures/d' $bookmarkConfig
cat $bookmarkConfig

# Set some convenient to use settings
dconf write /org/cinnamon/desktop/interface/first-day-of-week 1 # i.e. monday
dconf write /org/cinnamon/desktop/interface/clock-show-date true
dconf write /org/cinnamon/desktop/background/slideshow/slideshow-enabled true
dconf write /org/cinnamon/desktop/background/slideshow/image-source "'xml:///usr/share/cinnamon-background-properties/linuxmint-tina.xml'"

# Laptop specific settings
dconf write /org/cinnamon/desktop/screensaver/lock-enabled false
dconf write /org/cinnamon/settings-daemon/peripherals/touchpad/scrolling-method 2 # single finger scrolling
dconf write /org/cinnamon/settings-daemon/peripherals/touchpad/natural-scroll false # scroll direction

# Text editor (ex. Xed) configuration
dconf write /org/x/editor/preferences/editor/insert-spaces false
dconf write /org/x/editor/preferences/editor/bracket-matching true
dconf write /org/x/editor/preferences/editor/scheme "'cobalt'"
dconf write /org/x/editor/preferences/editor/highlight-current-line true
dconf write /org/x/editor/preferences/editor/display-line-numbers true
dconf write /org/x/editor/preferences/editor/auto-indent true

# Disable the unnecessary startup programs.
sudo rm /etc/xdg/autostart/mintreport.desktop

# Disable middle-click function for laptop touchpads
if true; then
	startupFileName=~/.config/autostart/disable-middle-click.desktop
	echo "[Desktop Entry]" > $startupFileName
	echo "Type=Application" >> $startupFileName
	echo "Exec=xmodmap -e \"pointer = 1 25 3 4 5 6 7 8 9\"" >> $startupFileName
	echo "X-GNOME-Autostart-enabled=true" >> $startupFileName
	echo "NoDisplay=false" >> $startupFileName
	echo "Hidden=false" >> $startupFileName
	echo "Name[en_US]=Disable middle click" >> $startupFileName
	echo "Comment[en_US]=No description" >> $startupFileName
	echo "X-GNOME-Autostart-Delay=2" >> $startupFileName
fi


# New files to install
installList=()

# Install 3rd party codecs (necessary for websites and playing video files)
# installList+=(mint-meta-codecs )
installList+=(gstreamer1.0-plugins-bad gstreamer1.0-libav evince )
# installList+=(mint-meta-codecs )

# Install typical scientific computation tools
if true; then
	# Install Octave. Second line provides some extra packages Octave complains about during image export.
	installList+=(octave liboctave-dev )
	installList+=(g++ )
	
	# Install Latex compiler
	installList+=(texlive-latex-base texlive-latex-recommended texlive-science texlive-latex-extra )
	installList+=(inkscape )	
fi

# Update the system and then remove the unpacked packages
sudo apt install ${installList[@]} -y
sudo apt dist-upgrade -y
sudo apt clean


# Generate public encryption key pair (necessary for server authentications)
if false; then
	sudo apt install autossh
	ssh-keygen -t rsa -b 4096 <<- EOF
	
	
	
	EOF
fi
