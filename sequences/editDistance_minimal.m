%%
% Returns a score based on the alignment quality. Lower scores are better, 0 indicates identity.
% Global alignment enforced

function out = editDistance_minimal(a,b)
	aSize = length(a);
	bSize = length(b);
	currentRow = 0:1:aSize;	
	for i=1:bSize
		upperRow = currentRow;
		currentRow(1) = i;
		for j=1:aSize
			if a(j)==b(i)
				currentRow(j+1) = min([ upperRow(j) upperRow(j+1)+1 currentRow(j)+1 ]);
			else
				currentRow(j+1) = min([ upperRow(j)+1 upperRow(j+1)+1 currentRow(j)+1 ]);
			end
		end
	end
	
	out = currentRow(end);
end



