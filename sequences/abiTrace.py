# A simple script to visualise ab1 chromatogram files.
# This is the file format for most Sanger sequencing facilities.
# See https://biopython.org/wiki/ABI_traces

from Bio import SeqIO
import matplotlib.pyplot as plt
import numpy as np

inputfilename = 'something.ab1'

# Import chromatogram file
record = SeqIO.read(inputfilename, 'abi')
record.annotations.keys()
record.annotations['abif_raw'].keys()
channels = ['DATA9', 'DATA10', 'DATA11', 'DATA12']

from collections import defaultdict
trace = defaultdict(list)
for c in channels:
	trace[c] = record.annotations['abif_raw'][c]

G = trace['DATA9']
A = trace['DATA10']
T = trace['DATA11']
C = trace['DATA12']


# Process the signal intensities (normalisation etc.)
baseStartPos = 1000
baseEndPos = 2000
A = np.array( A[baseStartPos:baseEndPos] )
C = np.array( C[baseStartPos:baseEndPos] )
G = np.array( G[baseStartPos:baseEndPos] )
T = np.array( T[baseStartPos:baseEndPos] )

total = A + C + G + T
maxSignal = np.amax(total).astype(np.float)
pA = 1e-8 + A / maxSignal
pC = 1e-8 + C / maxSignal
pG = 1e-8 + G / maxSignal
pT = 1e-8 + T / maxSignal


# Find peak positions in the chromatogram
totalPeaks = np.logical_and( total > np.roll(total,1), total > np.roll(total,-1) )
APeaks = np.logical_and( pA > np.roll(pA,1), pA > np.roll(pA,-1) )
CPeaks = np.logical_and( pC > np.roll(pC,1), pC > np.roll(pC,-1) )
GPeaks = np.logical_and( pG > np.roll(pG,1), pG > np.roll(pG,-1) )
TPeaks = np.logical_and( pT > np.roll(pT,1), pT > np.roll(pT,-1) )

threshold = 0.1
APeaks = 2*np.logical_and(APeaks, pA>threshold)-1
CPeaks = 2*np.logical_and(CPeaks, pC>threshold)-1
GPeaks = 2*np.logical_and(GPeaks, pG>threshold)-1
TPeaks = 2*np.logical_and(TPeaks, pT>threshold)-1


plt.plot(pA*APeaks, 'go')
plt.plot(pC*CPeaks, 'bo')
plt.plot(pG*GPeaks, 'ko')
plt.plot(pT*TPeaks, 'ro')

plt.plot(pA, color='green')
plt.plot(pC, color='blue')
plt.plot(pG, color='black')
plt.plot(pT, color='red')

plt.xlabel('Time (AU)')
plt.ylabel('Signal (AU)')
plt.ylim((0, 0.6))

plt.savefig('output.png', dpi=300)
#plt.show()
exit()

