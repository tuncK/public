%%
% Returns a score based on the alignment quality. Lower scores are better, 0 indicates identity.
% Global alignment enforced

function out = editDistance(a,b)
	aSize = length(a);
	bSize = length(b);
	
	table = nan(bSize+1,aSize+1)
	table(1,:) = 0:1:aSize;
	table(:,1) = 0:1:bSize;

	for i=1:1:bSize
		for j=1:1:aSize
			if a(j)==b(i)
				table(i+1,j+1) = min([ table(i,j) table(i,j+1)+1 table(i+1,j)+1 ]);
			else
				table(i+1,j+1) = min([ table(i,j)+1 table(i,j+1)+1 table(i+1,j)+1 ]);
			end
		end
	end
	
	out = table(end,end);
end


