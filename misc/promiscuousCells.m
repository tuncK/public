%%

% Random data to test, replace with the real one.
clusterIndeces = round(100*rand(100,10));
clusterIndeces(1:80,3) = clusterIndeces(1:80,4);


numCells = size(clusterIndeces,1);
numAttempts = size(clusterIndeces,2);
T = zeros(numCells,numCells,numAttempts);

for i=1:1:numAttempts
	numClusters = max(clusterIndeces(:,i));
	for j=0:numClusters
		idx = find(clusterIndeces(:,i)==j);
		if length(idx) >= 2
			pairs = nchoosek(idx,2);
			for k=1:length(pairs)
				T(pairs(1),pairs(2),i) = 1;
			end
		end
	end
end

changedCells = zeros(numAttempts,numAttempts);
for i=1:numAttempts
	for j=1:numAttempts
		changedCells(i,j) = sum(sum(abs(T(:,:,i)-T(:,:,j)) ));
	end
end

imagesc(changedCells)
colorbar
title('#cells changing clusters');
xlabel('Attempt #');
ylabel('Attempt #');

