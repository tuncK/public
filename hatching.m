% Introduces hatching pattern to a bar plot

function out = hatching ()
	colors2beHatched = [1,0,0]; % <Nx3 matrix of availableColorson the plot here>

	% in: Input image, imported as matrix.
	in = imread('before.png');
	out = in;

	pts = [' ', '/', '\', 'x', '+', 'c', '.', '-', '|' ];
	patternDim = 24;

	for i = 2:size(colors2beHatched,2);
		pattern = makehatch_plus(pts(i), patternDim, 12);
		color2replace = colors2beHatched(:,i);
		colorHit = (in(:,:,1) == color2replace(1)) & (in(:,:,2) == color2replace(2)) & (in(:,:,3) == color2replace(3));

		enlargedPattern = repmat(pattern, ceil(size(colorHit,1)/patternDim), ceil(size(colorHit,2)/patternDim) );
		enlargedPattern = enlargedPattern(1:size(colorHit,1), 1:size(colorHit,2) );
		hatchingToPut = logical(enlargedPattern .* colorHit);

		out += hatchingToPut*255;
	end

	imwrite(out, 'after.jpg', '-r300');
end



function A = makehatch_plus(hatch,n,m)
% Makes predefined hatch patterns
% From: https://www.mathworks.com/matlabcentral/fileexchange/24021-hatch-fill-patterns-plus-color-invert
%
%
% input (optional) N    size of hatch matrix (default = 6)
% input (optional) M    width of lines and dots in hatching (default = 1)
%
%  MAKEHATCH_PLUS(HATCH,N,M) returns a matrix with the hatch pattern for HATCH
%   according to the following table:
%      HATCH        pattern
%     -------      ---------
%        /          right-slanted lines
%        \          left-slanted lines
%        |          vertical lines
%        -          horizontal lines
%        +          crossing vertical and horizontal lines
%        x          criss-crossing lines
%        .          square dots
%        c          circular dots
%        w          Just a blank white pattern
%        k          Just a totally black pattern
%
%  See also: APPLYHATCH, APPLYHATCH_PLUS, APPLYHATCH_PLUSCOLOR, MAKEHATCH

%  By Ben Hinkle, bhinkle@mathworks.com
%  This code is in the public domain. 

% Modified Brian FG Katz    8-aout-03
% Modified David M Kaplan    19-fevrier-08

if ~exist('n','var'), n = 6; end
if ~exist('m','var'), m = 1; end
n=round(n);

switch (hatch)
  case '\'
    [B,C] = meshgrid( 0:n-1 );
    B = B-C; 
    clear C
    A = abs(B) <= m/2;
    A = A | abs(B-n) <= m/2;
    A = A | abs(B+n) <= m/2;
  case '/'
    A = fliplr(makehatch_plus('\',n,m));
  case '|'
    A=zeros(n);
    A(:,1:m) = 1;
  case '-'
    A = makehatch_plus('|',n,m);
    A = A';
  case '+'
    A = makehatch_plus('|',n,m);
    A = A | A';
  case 'x'
    A = makehatch_plus('\',n,m);
    A = A | fliplr(A);
  case '.'
    A=zeros(n);
    A(1:2*m,1:2*m)=1;
  case 'c'
    [B,C] = meshgrid( 0:n-1 );
    A = sqrt(B.^2+C.^2) <= m;
    A = A | fliplr(A) | flipud(A) | flipud(fliplr(A));
  case 'w'
    A = zeros(n);
  case 'k'
    A = ones(n);
  otherwise
    error(['Undefined hatch pattern "' hatch '".']);
end
